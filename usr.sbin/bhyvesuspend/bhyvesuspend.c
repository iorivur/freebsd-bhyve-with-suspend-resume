/*-
 * Copyright (c) 2011 NetApp, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY NETAPP, INC ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL NETAPP, INC OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD: head/usr.sbin/bhyvectl/bhyvectl.c 248935 2013-03-30 17:46:03Z neel $
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD: head/usr.sbin/bhyvectl/bhyvectl.c 248935 2013-03-30 17:46:03Z neel $");

#include <sys/param.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/errno.h>
#include <sys/mman.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <libutil.h>
#include <fcntl.h>
#include <string.h>
#include <getopt.h>
#include <assert.h>

#include <machine/vmm.h>
#include <vmmapi.h>

#define	MB	(1UL << 20)
#define	GB	(1UL << 30)

static const char *progname;

static void
usage(void)
{

	(void)fprintf(stderr,
	"Usage: %s [vmname]"
	"[filename]\n",
	progname);
	exit(1);
}


int
main(int argc, char *argv[])
{
	char *vmname = argv[1];
	int error;
	struct vmctx *ctx;
	struct vmstate *vmstate = malloc(sizeof(struct vmstate));
	struct vm_vcpustate *vcpu = malloc(sizeof(struct vm_vcpustate));

	if (argc != 3){
		usage();
		exit(EXIT_FAILURE);
	}

	error = 0;

	if (!error) {
		ctx = vm_open(vmname);
		if (ctx == NULL){
			error = -1;
			fprintf(stderr,"Not such virtual machine: %s\n", vmname);
			exit(EXIT_FAILURE);
		}
	}

	error = vm_get_allvstate(ctx,vmstate);
	printf("ret: %d\n", error);
	printf("errorno: %d\n", errno);

	printf("%lx,%lx\n",vcpu->regs->rax,vcpu->regs->rdx);
	exit(error);
}

